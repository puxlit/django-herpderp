from django.db import transaction
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.views.decorators.csrf import csrf_exempt

from herpderp.models import (Blip, BlipHistory, Player, PlayerHistory, Shard,
                             ShardHistory, Zone, ZoneHistory)
from herpderp.utils import (create_or_update, expect_api_key, expect_method,
                            expect_params, log_request)


@expect_method('GET')
@expect_api_key
def sitrep_view(request, shard_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    return render_to_response('herpderp/views/sitrep.html', {
        'api_key': request.GET['api_key'],
        'shard': shard,
    })


########################################
# Shard
########################################

def get_shard(shard_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    result = shard.as_dict()
    result['players'] = Player.objects.as_dict(shard=shard)
    result['blips'] = Blip.objects.as_dict(shard=shard)
    result['zones'] = Zone.objects.as_dict(shard=shard)
    return result


@expect_method('GET')
@expect_api_key
def get_shards_view(request):
    return JsonResponse(Shard.objects.as_dict())


@csrf_exempt
def dispatch_shard_view(request, *args, **kwargs):
    if (request.method == 'GET'):
        return get_shard_view(request, *args, **kwargs)
    elif (request.method == 'POST'):
        return create_or_update_shard_view(request, *args, **kwargs)
    else:
        raise Http404


@expect_method('GET')
@expect_api_key
def get_shard_view(request, shard_slug):
    return JsonResponse(get_shard(shard_slug))


@expect_method('POST')
@expect_api_key
@expect_params(['name', 'latitude', 'longitude', 'radius'])
@log_request
def create_or_update_shard_view(request, shard_slug):
    try:
        with transaction.atomic():
            create_or_update(
                Shard, {'slug': shard_slug}, {
                    'name': request.POST['name'],
                    'latitude': request.POST['latitude'],
                    'longitude': request.POST['longitude'],
                    'radius': request.POST['radius'],
                }, {
                }, ShardHistory, request.timestamp, request.ip_address
            )
    except:
        return JsonResponse({'success': False})
    return JsonResponse({
        'success': True,
        'result': get_shard(shard_slug),
    })


########################################
# Player
########################################

def get_player(shard_slug, player_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    player = get_object_or_404(Player, shard=shard, slug=player_slug)
    result = player.as_dict()
    result['blips'] = Blip.objects.as_dict(shard=shard,
                                           visible_to__in=[player])
    result['zones'] = Zone.objects.as_dict(shard=shard,
                                           visible_to__in=[player])
    return result


@expect_method('GET')
@expect_api_key
def get_players_view(request, shard_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    return JsonResponse(Player.objects.as_dict(shard=shard))


@csrf_exempt
def dispatch_player_view(request, *args, **kwargs):
    if (request.method == 'GET'):
        return get_player_view(request, *args, **kwargs)
    elif (request.method == 'POST'):
        return create_or_update_player_view(request, *args, **kwargs)
    else:
        raise Http404


@expect_method('GET')
@expect_api_key
def get_player_view(request, shard_slug, player_slug):
    return JsonResponse(get_player(shard_slug, player_slug))


@expect_method('POST')
@expect_api_key
@expect_params(['name', 'latitude', 'longitude', 'accuracy'])
@log_request
def create_or_update_player_view(request, shard_slug, player_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    try:
        with transaction.atomic():
            create_or_update(
                Player, {'shard': shard, 'slug': player_slug}, {
                    'name': request.POST['name'],
                    'latitude': request.POST['latitude'],
                    'longitude': request.POST['longitude'],
                    'accuracy': request.POST['accuracy'],
                }, {
                }, PlayerHistory, request.timestamp, request.ip_address
            )
    except:
        return JsonResponse({'success': False})
    return JsonResponse({
        'success': True,
        'result': get_player(shard_slug, player_slug),
    })


########################################
# Blip
########################################

def get_blip(shard_slug, blip_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    blip = get_object_or_404(Blip, shard=shard, slug=blip_slug)
    result = blip.as_dict()
    result['players'] = Player.objects.as_dict(pk__in=blip.visible_to
                                                          .values_list('pk'))
    return result


@expect_method('GET')
@expect_api_key
def get_blips_view(request, shard_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    return JsonResponse(Blip.objects.as_dict(shard=shard))


@csrf_exempt
def dispatch_blip_view(request, *args, **kwargs):
    if (request.method == 'GET'):
        return get_blip_view(request, *args, **kwargs)
    elif (request.method == 'POST'):
        return create_or_update_blip_view(request, *args, **kwargs)
    else:
        raise Http404


@expect_method('GET')
@expect_api_key
def get_blip_view(request, shard_slug, blip_slug):
    return JsonResponse(get_blip(shard_slug, blip_slug))


@expect_method('POST')
@expect_api_key
@expect_params(['name', 'latitude', 'longitude'])
@log_request
def create_or_update_blip_view(request, shard_slug, blip_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    try:
        with transaction.atomic():
            create_or_update(
                Blip, {'shard': shard, 'slug': blip_slug}, {
                    'name': request.POST['name'],
                    'latitude': request.POST['latitude'],
                    'longitude': request.POST['longitude'],
                }, {
                    'visible_to': [{'shard': shard, 'slug': value} for value
                                   in request.POST.getlist('visible_to')],
                }, BlipHistory, request.timestamp, request.ip_address
            )
    except:
        return JsonResponse({'success': False})
    return JsonResponse({
        'success': True,
        'result': get_blip(shard_slug, blip_slug),
    })


########################################
# Zone
########################################

def get_zone(shard_slug, zone_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    zone = get_object_or_404(Zone, shard=shard, slug=zone_slug)
    result = zone.as_dict()
    result['players'] = Player.objects.as_dict(pk__in=zone.visible_to
                                                          .values_list('pk'))
    return result


@expect_method('GET')
@expect_api_key
def get_zones_view(request, shard_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    return JsonResponse(Zone.objects.as_dict(shard=shard))


@csrf_exempt
def dispatch_zone_view(request, *args, **kwargs):
    if (request.method == 'GET'):
        return get_zone_view(request, *args, **kwargs)
    elif (request.method == 'POST'):
        return create_or_update_zone_view(request, *args, **kwargs)
    else:
        raise Http404


@expect_method('GET')
@expect_api_key
def get_zone_view(request, shard_slug, zone_slug):
    return JsonResponse(get_zone(shard_slug, zone_slug))


@expect_method('POST')
@expect_api_key
@expect_params(['name', 'latitude', 'longitude', 'radius'])
@log_request
def create_or_update_zone_view(request, shard_slug, zone_slug):
    shard = get_object_or_404(Shard, slug=shard_slug)
    try:
        with transaction.atomic():
            create_or_update(
                Zone, {'shard': shard, 'slug': zone_slug}, {
                    'name': request.POST['name'],
                    'latitude': request.POST['latitude'],
                    'longitude': request.POST['longitude'],
                    'radius': request.POST['radius'],
                }, {
                    'visible_to': [{'shard': shard, 'slug': value} for value
                                   in request.POST.getlist('visible_to')],
                }, ZoneHistory, request.timestamp, request.ip_address
            )
    except:
        return JsonResponse({'success': False})
    return JsonResponse({
        'success': True,
        'result': get_zone(shard_slug, zone_slug),
    })

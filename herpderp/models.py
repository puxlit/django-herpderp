from django.db import models

from herpderp.utils import Locatable, Loggable


########################################
# Shard
########################################

class AbstractShard(Locatable):
    slug = models.SlugField(max_length=8)
    name = models.CharField(max_length=32)
    radius = models.DecimalField(max_digits=7, decimal_places=1)

    class Meta:
        abstract = True


class ShardManager(models.Manager):
    def as_dict(self, **kwargs):
        return {shard.slug: shard.as_dict() for shard
                in Shard.objects.filter(**kwargs)}


class Shard(AbstractShard):
    objects = ShardManager()

    class Meta:
        unique_together = ('slug',)

    def as_dict(self):
        return {
            'name': self.name,
            'latitude': float(self.latitude),
            'longitude': float(self.longitude),
            'radius': float(self.radius),
        }


class ShardHistory(AbstractShard, Loggable):
    class Meta:
        unique_together = ('slug', 'timestamp')


########################################
# Player
########################################

class AbstractPlayer(Locatable):
    shard = models.ForeignKey('Shard')
    slug = models.SlugField(max_length=8)
    name = models.CharField(max_length=32)
    accuracy = models.DecimalField(max_digits=4, decimal_places=1)

    class Meta:
        abstract = True


class PlayerManager(models.Manager):
    def as_dict(self, **kwargs):
        return {player.slug: player.as_dict() for player
                in Player.objects.filter(**kwargs)}


class Player(AbstractPlayer):
    objects = PlayerManager()

    class Meta:
        unique_together = ('shard', 'slug')

    def as_dict(self):
        return {
            'name': self.name,
            'latitude': float(self.latitude),
            'longitude': float(self.longitude),
            'accuracy': float(self.accuracy),
        }


class PlayerHistory(AbstractPlayer, Loggable):
    class Meta:
        unique_together = ('shard', 'slug', 'timestamp')


########################################
# Blip
########################################

class AbstractBlip(Locatable):
    shard = models.ForeignKey('Shard')
    slug = models.SlugField(max_length=8)
    name = models.CharField(max_length=32)
    visible_to = models.ManyToManyField('Player')

    class Meta:
        abstract = True


class BlipManager(models.Manager):
    def as_dict(self, **kwargs):
        return {blip.slug: blip.as_dict() for blip
                in Blip.objects.filter(**kwargs)}


class Blip(AbstractBlip):
    objects = BlipManager()

    class Meta:
        unique_together = ('shard', 'slug')

    def as_dict(self):
        return {
            'name': self.name,
            'latitude': float(self.latitude),
            'longitude': float(self.longitude),
            'visible_to': list(self.visible_to.values_list('slug', flat=True)),
        }


class BlipHistory(AbstractBlip, Loggable):
    class Meta:
        unique_together = ('shard', 'slug', 'timestamp')


########################################
# Zone
########################################

class AbstractZone(Locatable):
    shard = models.ForeignKey('Shard')
    slug = models.SlugField(max_length=8)
    name = models.CharField(max_length=32)
    radius = models.DecimalField(max_digits=7, decimal_places=1)
    visible_to = models.ManyToManyField('Player')

    class Meta:
        abstract = True


class ZoneManager(models.Manager):
    def as_dict(self, **kwargs):
        return {zone.slug: zone.as_dict() for zone
                in Zone.objects.filter(**kwargs)}


class Zone(AbstractZone):
    objects = ZoneManager()

    class Meta:
        unique_together = ('shard', 'slug')

    def as_dict(self):
        return {
            'name': self.name,
            'latitude': float(self.latitude),
            'longitude': float(self.longitude),
            'radius': float(self.radius),
            'visible_to': list(self.visible_to.values_list('slug', flat=True)),
        }


class ZoneHistory(AbstractZone, Loggable):
    class Meta:
        unique_together = ('shard', 'slug', 'timestamp')

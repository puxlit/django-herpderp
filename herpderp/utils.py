from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.http import Http404

from datetime import datetime, timezone


########################################
# Abstract Base Classes for Models
########################################

class Locatable(models.Model):
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)

    class Meta:
        abstract = True


class Loggable(models.Model):
    timestamp = models.DateTimeField()
    ip_address = models.GenericIPAddressField()

    class Meta:
        abstract = True


########################################
# Decorators for Views
########################################

def expect_kv(key, value):
    def wrap(func):
        def wrapped(request, *args, **kwargs):
            if not (request.GET.get(key, None) == value):
                raise Http404
            return func(request, *args, **kwargs)
        return wrapped
    return wrap

expect_api_key = expect_kv('api_key', settings.HERPDERP_API_KEY)


def expect_method(method):
    def wrap(func):
        def wrapped(request, *args, **kwargs):
            if not (request.method == method):
                raise Http404
            return func(request, *args, **kwargs)
        return wrapped
    return wrap


def expect_params(params):
    def wrap(func):
        def wrapped(request, *args, **kwargs):
            if not all(param in request.POST for param in params):
                raise Http404
            return func(request, *args, **kwargs)
        return wrapped
    return wrap


def log_request(func):
    def wrapped(request, *args, **kwargs):
        timestamp = datetime.now(timezone.utc)
        if 'HTTP_X_FORWARDED_FOR' in request.META:
            ip_address = request.META['HTTP_X_FORWARDED_FOR'].split(',')[0] \
                                .strip()
        elif 'REMOTE_ADDR' in request.META:
            ip_address = request.META['REMOTE_ADDR'].strip()
        else:
            ip_address = '0.0.0.0'
        request.timestamp = timestamp
        request.ip_address = ip_address
        return func(request, *args, **kwargs)
    return wrapped


########################################
# Miscellaneous
########################################

def create_or_update(model, natural_key, remaining_fields, m2m_fields,
                     history_model, timestamp, ip_address):
    all_fields = dict(natural_key, **remaining_fields)

    try:
        entity = model.objects.get(**natural_key)
        for (field, value) in remaining_fields.items():
            setattr(entity, field, value)
    except ObjectDoesNotExist:
        entity = model(**all_fields)
    entity.full_clean()
    entity.save()

    for (field, values) in m2m_fields.items():
        set_m2m_field(entity, field, values)

    history_entity = history_model(timestamp=timestamp, ip_address=ip_address,
                                   **all_fields)
    history_entity.full_clean()
    history_entity.save()

    for (field, values) in m2m_fields.items():
        set_m2m_field(history_entity, field, values)

    return (entity, history_entity)


def set_m2m_field(entity, field, related_entities_by_natural_key):
    relation = getattr(entity, field)
    related_entities = [relation.model.objects.get(**natural_key)
                        for natural_key in related_entities_by_natural_key]
    relation.clear()
    relation.add(*related_entities)

from django.conf.urls import patterns, url

from herpderp.views import (dispatch_blip_view, dispatch_player_view,
                            dispatch_shard_view, dispatch_zone_view,
                            get_blips_view, get_players_view, get_shards_view,
                            get_zones_view, sitrep_view)

urlpatterns = patterns('',
    url(r'^sitrep/(?P<shard_slug>[\w-]{1,8})$',
        sitrep_view, name='sitrep'),
    url(r'^api/v1/shards/$',
        get_shards_view, name='get_shards'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})$',
        dispatch_shard_view, name='dispatch_shard'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/players/$',
        get_players_view, name='get_players'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/players/(?P<player_slug>[\w-]{1,8})$',
        dispatch_player_view, name='dispatch_player'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/blips/$',
        get_blips_view, name='get_blips'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/blips/(?P<blip_slug>[\w-]{1,8})$',
        dispatch_blip_view, name='dispatch_blip'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/zones/$',
        get_zones_view, name='get_zones'),
    url(r'^api/v1/shards/(?P<shard_slug>[\w-]{1,8})/zones/(?P<zone_slug>[\w-]{1,8})$',
        dispatch_zone_view, name='dispatch_zone'),
)

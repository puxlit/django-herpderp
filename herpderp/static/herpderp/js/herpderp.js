$(function () {
    'use strict';

    /************************************/
    /* BEGIN SCOPE VARIABLE DEFINITIONS */

    var api_base_path, api_key;

    api_base_path = $('body').data('api-base-path');
    api_key = $('body').data('api-key');

    /*  END SCOPE VARIABLE DEFINITIONS  */
    /************************************/

    /************************************/
    /* BEGIN SCOPE FUNCTION DEFINITIONS */

    function fetchUpdate(callback) {
        $.getJSON(api_base_path, {'api_key': api_key}, callback);
    }

    /*  END SCOPE FUNCTION DEFINITIONS  */
    /************************************/

    /************************************/
    /* BEGIN INITIAL BOOTSTRAP SEQUENCE */

    fetchUpdate(function (newShard) {
        /************************************/
        /* BEGIN SCOPE VARIABLE DEFINITIONS */

        var artefacts, doubleClickTimeout, inspector, map, shard;

        artefacts = [];
        doubleClickTimeout = null;
        inspector = null;
        shard = newShard;

        map = new google.maps.Map(document.getElementById('map'), {
            'center': new google.maps.LatLng(shard.latitude, shard.longitude),
            'disableDoubleClickZoom': true,
            'zoom': 13 // Coerce the scale control into displaying a one kilometre yardstick.
        });

        /*  END SCOPE VARIABLE DEFINITIONS  */
        /************************************/

        /************************************/
        /* BEGIN SCOPE FUNCTION DEFINITIONS */

        function createArtefact(type, slug, entity, animate, editable) {
            var ArtefactConstructor, artefact, options;

            options = {
                'map': map
            };

            switch (type) {
            case 'shard':
            case 'zone':
                ArtefactConstructor = google.maps.Circle;
                $.extend(options, {
                    'center': new google.maps.LatLng(entity.latitude, entity.longitude),
                    'fillColor': '#f00',
                    'radius': entity.radius,
                    'strokeWeight': 0
                });
                break;
            case 'player':
            case 'blip':
                ArtefactConstructor = google.maps.Marker;
                $.extend(options, {
                    'position': new google.maps.LatLng(entity.latitude, entity.longitude)
                });
                break;
            }

            switch (type) {
            case 'shard':
                $.extend(options, {
                    'fillOpacity': 0.1,
                    'zIndex': 0
                });
                break;
            case 'zone':
                $.extend(options, {
                    'fillOpacity': 0.4,
                    'zIndex': 1
                });
                break;
            case 'player':
                $.extend(options, {
                    'zIndex': 2
                });
                break;
            case 'blip':
                $.extend(options, {
                    'zIndex': 3
                });
                break;
            }

            if (animate) {
                $.extend(options, {
                    'animation': google.maps.Animation.DROP
                });
            }

            if (editable) {
                switch (type) {
                case 'shard':
                case 'zone':
                    $.extend(options, {
                        'editable': true
                    });
                    // Fall through to next case.
                case 'player':
                case 'blip':
                    $.extend(options, {
                        'draggable': true,
                        'zIndex': 5
                    });
                    break;
                }
            }

            artefact = new ArtefactConstructor(options);
            artefact.herpderp = {
                'type': type,
                'slug': slug,
                'listeners': []
            };

            return artefact;
        }

        function destroyArtefact(artefact) {
            var i;

            for (i in artefact.herpderp.listeners) {
                if (artefact.herpderp.listeners.hasOwnProperty(i)) {
                    google.maps.event.removeListener(artefact.herpderp.listeners[i]);
                }
            }

            artefact.setMap(null);
        }

        function quadrupleArea(latLngBounds) {
            return new google.maps.LatLngBounds(
                new google.maps.LatLng(
                    latLngBounds.getCenter().lat() + (2 * (latLngBounds.getSouthWest().lat() - latLngBounds.getCenter().lat())),
                    latLngBounds.getCenter().lng() + (2 * (latLngBounds.getSouthWest().lng() - latLngBounds.getCenter().lng()))
                ),
                new google.maps.LatLng(
                    latLngBounds.getCenter().lat() + (2 * (latLngBounds.getNorthEast().lat() - latLngBounds.getCenter().lat())),
                    latLngBounds.getCenter().lng() + (2 * (latLngBounds.getNorthEast().lng() - latLngBounds.getCenter().lng()))
                )
            );
        }

        function setupInspector(event, artefact) {
            /************************************/
            /* BEGIN SCOPE FUNCTION DEFINITIONS */

            function updateInspectorInfoWindow(updatePosition, updateRadius) {
                /************************************/
                /* BEGIN SCOPE VARIABLE DEFINITIONS */

                var infoWindowHTML;

                /*  END SCOPE VARIABLE DEFINITIONS  */
                /************************************/

                /************************************/
                /* BEGIN SCOPE FUNCTION DEFINITIONS */

                function fromDecimalToDMS(decimal, isLatitude) {
                    /************************************/
                    /* BEGIN SCOPE VARIABLE DEFINITIONS */

                    var df, di, mf, mi, sf, suffix;

                    /*  END SCOPE VARIABLE DEFINITIONS  */
                    /************************************/

                    /************************************/
                    /* BEGIN SCOPE FUNCTION DEFINITIONS */

                    function truncate(x) {
                        return ((x < 0) ? Math.ceil(x) : Math.floor(x));
                    }

                    /*  END SCOPE FUNCTION DEFINITIONS  */
                    /************************************/

                    /************************************/
                    /* BEGIN THE ACTUAL METHOD SEQUENCE */

                    df = Math.abs(decimal);
                    di = truncate(df);
                    mf = (df - di) * 60;
                    mi = truncate(mf);
                    sf = (mf - mi) * 60;

                    if (decimal > 0) {
                        suffix = '\u00a0' + (isLatitude ? 'N' : 'E');
                    } else if (decimal < 0) {
                        suffix = '\u00a0' + (isLatitude ? 'S' : 'W');
                    } else if (decimal === 0) {
                        suffix = '';
                    }

                    return di.toString() + '\u00b0\u00a0' + mi.toString() + '\u2032\u00a0' + sf.toFixed(3) + '\u2033' + suffix;

                    /*  END THE ACTUAL METHOD SEQUENCE  */
                    /************************************/
                }

                function fromDecimalToMetres(decimal) {
                    return decimal.toFixed(1) + '\u00a0m';
                }

                function generateVisibleToHTML(players, visibleTo) {
                    var slug, sortedSlugs, visibleToHTML;

                    sortedSlugs = [];
                    for (slug in players) {
                        if (players.hasOwnProperty(slug)) {
                            sortedSlugs.push(slug);
                        }
                    }
                    sortedSlugs.sort();
                    sortedSlugs.reverse();

                    visibleToHTML = '';
                    while (sortedSlugs.length > 0) {
                        slug = sortedSlugs.pop();

                        visibleToHTML += '<div class="checkbox"><label>';
                        visibleToHTML += '<input type="checkbox" name="visible_to" value="' + slug + '"' + ((visibleTo.indexOf(slug) === -1) ? '' : ' checked="checked"') + '/>';
                        visibleToHTML += '<span>' + players[slug].name + '</span>';
                        visibleToHTML += '</label></div>';
                    }

                    return visibleToHTML;
                }

                /*  END SCOPE FUNCTION DEFINITIONS  */
                /************************************/

                /************************************/
                /* BEGIN THE ACTUAL METHOD SEQUENCE */

                if (!updatePosition && !updateRadius) {

                    infoWindowHTML = '<div class="info-window">';
                    if (inspector.targetArtefact !== null) {
                        switch (inspector.entity.type) {
                        case 'shard':
                            infoWindowHTML += '<h4>Shard ' + inspector.entity.details.name + '</h4>';
                            break;
                        case 'zone':
                            infoWindowHTML += '<h4>Zone ' + inspector.entity.details.name + '</h4>';
                            break;
                        case 'player':
                            infoWindowHTML += '<h4>Player ' + inspector.entity.details.name + '</h4>';
                            break;
                        case 'blip':
                            infoWindowHTML += '<h4>Blip ' + inspector.entity.details.name + '</h4>';
                            break;
                        }
                    } else {
                        infoWindowHTML += '<h4>New artefact</h4>';
                    }
                    infoWindowHTML += '<table class="table table-condensed">';
                    if (inspector.targetArtefact === null) {
                        infoWindowHTML += '<tr><th>Type</th><td><div class="radio"><label><input type="radio" name="type" value="blip"' + ((inspector.entity.type === 'blip') ? ' checked="checked"' : '') + '><span>Blip</span></label></div><div class="radio"><label><input type="radio" name="type" value="zone"' + ((inspector.entity.type === 'zone') ? ' checked="checked"' : '') + '><span>Zone</span></label></div></td></tr>';
                        infoWindowHTML += '<tr><th>Slug</th><td><input class="form-control input-sm" type="text" name="slug"/></td></tr>';
                        infoWindowHTML += '<tr><th>Name</th><td><input class="form-control input-sm" type="text" name="name"/></td></tr>';
                    }
                    infoWindowHTML += '<tr><th>Latitude</th><td id="inspector-latitude-value">' + fromDecimalToDMS(inspector.entity.details.latitude, true) + '</td></tr>';
                    infoWindowHTML += '<tr><th>Longitude</th><td id="inspector-longitude-value">' + fromDecimalToDMS(inspector.entity.details.longitude, false) + '</td></tr>';
                    switch (inspector.entity.type) {
                    case 'shard':
                        infoWindowHTML += '<tr><th>Radius</th><td id="inspector-radius-value">' + fromDecimalToMetres(inspector.entity.details.radius) + '</td></tr>';
                        break;
                    case 'zone':
                        infoWindowHTML += '<tr><th>Radius</th><td id="inspector-radius-value">' + fromDecimalToMetres(inspector.entity.details.radius) + '</td></tr>';
                        infoWindowHTML += '<tr><th>Visible to</th><td>' + generateVisibleToHTML(shard.players, inspector.entity.details.visible_to) + '</td></tr>';
                        break;
                    case 'player':
                        infoWindowHTML += '<tr><th>Accuracy</th><td>' + fromDecimalToMetres(inspector.entity.details.accuracy) + '</td></tr>';
                        break;
                    case 'blip':
                        infoWindowHTML += '<tr><th>Visible to</th><td>' + generateVisibleToHTML(shard.players, inspector.entity.details.visible_to) + '</td></tr>';
                        break;
                    }
                    infoWindowHTML += '</table>';
                    if (inspector.entity.type !== 'player') {
                        infoWindowHTML += '<button id="inspector-cancel-button" class="btn btn-default btn-sm">Cancel</button><button id="inspector-save-button" class="btn btn-primary btn-sm pull-right">Save</button>';
                    }
                    infoWindowHTML += '</div>';

                    inspector.infoWindow.setContent($.parseHTML(infoWindowHTML)[0]);
                    inspector.infoWindow.open(map, inspector.proxyArtefact);

                } else {

                    if (updatePosition) {
                        $('#inspector-latitude-value').text(fromDecimalToDMS(inspector.entity.details.latitude, true));
                        $('#inspector-longitude-value').text(fromDecimalToDMS(inspector.entity.details.longitude, false));
                    }

                    if (updateRadius) {
                        $('#inspector-radius-value').text(fromDecimalToMetres(inspector.entity.details.radius));
                    }

                }

                /*  END THE ACTUAL METHOD SEQUENCE  */
                /************************************/
            }

            function updateInspectorProxyArtefact(firstUpdate) {
                if (inspector.proxyArtefact !== undefined) {
                    destroyArtefact(inspector.proxyArtefact);
                    delete inspector.proxyArtefact;
                }

                inspector.proxyArtefact = createArtefact(inspector.entity.type, inspector.entity.slug, inspector.entity.details, (inspector.targetArtefact === null) && firstUpdate, (inspector.entity.type !== 'player'));

                switch (inspector.entity.type) {
                case 'shard':
                case 'zone':
                    inspector.proxyArtefact.getPosition = inspector.proxyArtefact.getCenter;
                    inspector.proxyArtefact.gm_bindings_.position = inspector.proxyArtefact.gm_bindings_.center; // Invoke black magic; this will backfire (cosmetically) when Google change their internals.
                    inspector.proxyArtefact.herpderp.listeners.push(google.maps.event.addListener(inspector.proxyArtefact, 'center_changed', function () {
                        google.maps.event.trigger(inspector.proxyArtefact, 'position_changed');
                    }));
                    inspector.proxyArtefact.herpderp.listeners.push(google.maps.event.addListener(inspector.proxyArtefact, 'radius_changed', function () {
                        inspector.entity.details.radius = inspector.proxyArtefact.getRadius();
                        updateInspectorInfoWindow(false, true);
                    }));
                    // Fall through to next case.
                case 'player':
                case 'blip':
                    inspector.proxyArtefact.herpderp.listeners.push(google.maps.event.addListener(inspector.proxyArtefact, 'position_changed', function () {
                        inspector.entity.details.latitude = inspector.proxyArtefact.getPosition().lat();
                        inspector.entity.details.longitude = inspector.proxyArtefact.getPosition().lng();
                        updateInspectorInfoWindow(true, false);
                    }));
                    break;
                }
            }

            function teardownInspector(updateHook) {
                /************************************/
                /* BEGIN SCOPE FUNCTION DEFINITIONS */

                function continuation() {
                    // [I4] Destroy proxy artefact.
                    destroyArtefact(inspector.proxyArtefact);
                    delete inspector.proxyArtefact;

                    // [I3] Hide frost overlay.
                    inspector.frostOverlay.setMap(null);
                    delete inspector.frostOverlay;

                    // [I2] Enable update button.
                    $('#update-button').prop('disabled', false);

                    // [I1] Destroy inspector object.
                    inspector = null;
                }

                /*  END SCOPE FUNCTION DEFINITIONS  */
                /************************************/

                /************************************/
                /* BEGIN THE ACTUAL METHOD SEQUENCE */

                // [I6] Destroy info window.
                inspector.infoWindow = null;

                // [I5] Show target artefact.
                if (updateHook === undefined) {
                    if (inspector.targetArtefact !== null) {
                        inspector.targetArtefact.setVisible(true);
                    }
                    continuation();
                } else {
                    updateHook(continuation);
                }

                /*  END THE ACTUAL METHOD SEQUENCE  */
                /************************************/
            }

            /*  END SCOPE FUNCTION DEFINITIONS  */
            /************************************/

            /************************************/
            /* BEGIN THE ACTUAL METHOD SEQUENCE */

            // [I1] Create inspector object.
            inspector = {
                'initiatingEvent': event,
                'targetArtefact': artefact,
                'update': function () {
                    updateInspectorProxyArtefact(false);
                    updateInspectorInfoWindow(false, false);
                }
            };
            if (inspector.targetArtefact !== null) {
                if (inspector.targetArtefact.herpderp.type === 'shard') {
                    inspector.entity = {
                        'type': 'shard',
                        'slug': null,
                        'details': {
                            'name': shard.name,
                            'latitude': shard.latitude,
                            'longitude': shard.longitude,
                            'radius': shard.radius
                        }
                    };
                } else {
                    inspector.entity = {
                        'type': inspector.targetArtefact.herpderp.type,
                        'slug': inspector.targetArtefact.herpderp.slug,
                        'details': $.extend({}, shard[inspector.targetArtefact.herpderp.type + 's'][inspector.targetArtefact.herpderp.slug])
                    };
                }
            } else {
                inspector.entity = {
                    'type': 'blip',
                    'slug': '',
                    'details': {
                        'name': '',
                        'latitude': event.latLng.lat(),
                        'longitude': event.latLng.lng(),
                        'visible_to': []
                    }
                };
            }

            // [I2] Disable update button.
            $('#update-button').prop('disabled', true);

            // [I3] Show frost overlay.
            // This is ridiculously dodgy; it'll break if you zoom out too far.
            inspector.frostOverlay = new google.maps.Rectangle({
                'bounds': quadrupleArea(map.getBounds()),
                'fillColor': '#000',
                'fillOpacity': 0.25,
                'map': map,
                'strokeWeight': 0,
                'zIndex': 4
            });

            // [I4] Create proxy artefact.
            updateInspectorProxyArtefact(true);

            // [I5] Hide target artefact.
            if (inspector.targetArtefact !== null) {
                inspector.targetArtefact.setVisible(false);
            }

            // [I6] Create info window.
            inspector.infoWindow = new google.maps.InfoWindow({
                'zIndex': 6
            });
            updateInspectorInfoWindow(false, false);
            google.maps.event.addListenerOnce(inspector.infoWindow, 'closeclick', teardownInspector);

            /*  END THE ACTUAL METHOD SEQUENCE  */
            /************************************/
        }

        function drawUpdate(callback) {
            /************************************/
            /* BEGIN SCOPE VARIABLE DEFINITIONS */

            var artefact, bounds, i;

            /*  END SCOPE VARIABLE DEFINITIONS  */
            /************************************/

            /************************************/
            /* BEGIN SCOPE FUNCTION DEFINITIONS */

            function wireArtefact(artefact) {
                /************************************/
                /* BEGIN SCOPE FUNCTION DEFINITIONS */

                function handleArtefactClicks(event, artefact, firstClick) {
                    if (doubleClickTimeout !== null) {
                        window.clearTimeout(doubleClickTimeout);
                        doubleClickTimeout = null;
                    }

                    if (inspector === null) {
                        if (firstClick) {
                            doubleClickTimeout = window.setTimeout(function () { setupInspector(event, artefact); }, 250);
                        } else {
                            google.maps.event.trigger(map, 'dblclick', event);
                        }
                    }
                }

                /*  END SCOPE FUNCTION DEFINITIONS  */
                /************************************/

                /************************************/
                /* BEGIN THE ACTUAL METHOD SEQUENCE */

                artefact.herpderp.listeners.push(google.maps.event.addListener(artefact, 'click', function (event) { handleArtefactClicks(event, artefact, true); }));
                artefact.herpderp.listeners.push(google.maps.event.addListener(artefact, 'dblclick', function (event) { handleArtefactClicks(event, artefact, false); }));

                /*  END THE ACTUAL METHOD SEQUENCE  */
                /************************************/
            }

            /*  END SCOPE FUNCTION DEFINITIONS  */
            /************************************/

            /************************************/
            /* BEGIN THE ACTUAL METHOD SEQUENCE */

            // Remove existing artefacts.
            for (i in artefacts) {
                if (artefacts.hasOwnProperty(i)) {
                    destroyArtefact(artefacts[i]);
                }
            }
            artefacts = [];

            // Add shard artefact.
            artefact = createArtefact('shard', null, shard, false, false);
            wireArtefact(artefact);
            artefacts.push(artefact);
            bounds = artefact.getBounds();

            // Add zone artefacts.
            $.each(shard.zones, function (zone_slug, zone) {
                artefact = createArtefact('zone', zone_slug, zone, false, false);
                wireArtefact(artefact);
                artefacts.push(artefact);
                bounds.union(artefact.getBounds());
            });

            // Add player artefacts.
            $.each(shard.players, function (player_slug, player) {
                artefact = createArtefact('player', player_slug, player, false, false);
                wireArtefact(artefact);
                artefacts.push(artefact);
                bounds.extend(artefact.getPosition());
            });

            // Add blip artefacts.
            $.each(shard.blips, function (blip_slug, blip) {
                artefact = createArtefact('blip', blip_slug, blip, false, false);
                wireArtefact(artefact);
                artefacts.push(artefact);
                bounds.extend(artefact.getPosition());
            });

            // Fit viewport to encompass all artefacts.
            map.fitBounds(bounds);

            // Trigger callback.
            callback((new Date()).toISOString());

            /*  END THE ACTUAL METHOD SEQUENCE  */
            /************************************/
        }

        function fetchAndDrawUpdate(callback) {
            $('#update-button').prop('disabled', true).html('Updating\u2026');
            google.maps.event.trigger(map, 'resize');

            fetchUpdate(function (newShard) {
                shard = newShard;

                drawUpdate(function (timestamp) {
                    $('#update-status').html('Last updated on ' + timestamp);
                    $('#update-button').html('Update');
                    google.maps.event.trigger(map, 'resize');

                    if (callback === undefined) {
                        $('#update-button').prop('disabled', false);
                    } else {
                        callback();
                    }
                });
            });
        }

        /*  END SCOPE FUNCTION DEFINITIONS  */
        /************************************/

        /************************************/
        /* BEGIN INITIAL BOOTSTRAP SEQUENCE */

        drawUpdate(function (timestamp) {
            // Prepare update control.
            map.controls[google.maps.ControlPosition.TOP_CENTER].push($.parseHTML('<div id="update-bar" class="well well-sm"><span id="update-status">Last updated on ' + timestamp + '</span><button id="update-button" type="button" class="btn btn-primary btn-xs">Update</button></div>')[0]);
            $('#map').on('click', '#update-button', function () {
                if (inspector === null) {
                    fetchAndDrawUpdate();
                }
            });

            // Wire artefact creation.
            google.maps.event.addListener(map, 'dblclick', function (event) {
                if (inspector === null) {
                    setupInspector(event, null);
                }
            });

            // Wire inspector controls.
            google.maps.event.addListener(map, 'bounds_changed', function () {
                if (inspector !== null) {
                    // This is ridiculously dodgy; it'll break if you zoom out too far.
                    inspector.frostOverlay.setBounds(quadrupleArea(map.getBounds()));
                }
            });
            $('#map').on('change', 'input[type="radio"][name="type"]', function () {
                if (inspector !== null) {
                    inspector.entity.type = $('#map input[type="radio"][name="type"]:checked').val();
                    switch (inspector.entity.type) {
                    case 'blip':
                        delete inspector.entity.details.radius;
                        inspector.update();
                        break;
                    case 'zone':
                        inspector.entity.details.radius = 50;
                        inspector.update();
                        break;
                    }
                }
            });
            $('#map').on('change', 'input[type="text"][name="slug"]', function () {
                if (inspector !== null) {
                    inspector.entity.slug = $('#map input[type="text"][name="slug"]').val();
                }
            });
            $('#map').on('change', 'input[type="text"][name="name"]', function () {
                if (inspector !== null) {
                    inspector.entity.details.name = $('#map input[type="text"][name="name"]').val();
                }
            });
            $('#map').on('change', 'input[type="checkbox"][name="visible_to"]', function () {
                if (inspector !== null) {
                    inspector.entity.details.visible_to = $('#map input[type="checkbox"][name="visible_to"]:checked').map(function () {
                        return $(this).prop('value');
                    }).get();
                }
            });
            $('#map').on('click', '#inspector-cancel-button', function () {
                if (inspector !== null) {
                    inspector.infoWindow.close();
                    google.maps.event.trigger(inspector.infoWindow, 'closeclick');
                }
            });
            $('#map').on('click', '#inspector-save-button', function () {
                var api_path;

                if (inspector !== null) {
                    $('#inspector-cancel-button').prop('disabled', true);
                    $('#inspector-save-button').prop('disabled', true).html('Saving\u2026');

                    api_path = api_base_path;
                    if (inspector.entity.type !== 'shard') {
                        api_path += '/' + inspector.entity.type + 's/' + inspector.entity.slug;
                    }
                    api_path += '?api_key=' + api_key;

                    $.ajax({
                        'type': 'POST',
                        'url': api_path,
                        'traditional': true, // Serialise arrays the right way.
                        'data': inspector.entity.details,
                        'dataType': 'json',
                        'success': function () {
                            inspector.infoWindow.close();
                            google.maps.event.trigger(inspector.infoWindow, 'closeclick', fetchAndDrawUpdate);
                        }
                    });
                }
            });
        });

        /*  END INITIAL BOOTSTRAP SEQUENCE  */
        /************************************/
    });

    /*  END INITIAL BOOTSTRAP SEQUENCE  */
    /************************************/
});
